FROM debian:11 as builder

ARG MMPROXY_REF=2018.7.0

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y \
            git \
            make autoconf automake libtool \
            gcc clang

RUN git clone --depth 1 --branch $MMPROXY_REF https://github.com/cloudflare/mmproxy.git /mmproxy

WORKDIR /mmproxy

RUN git submodule update --init

RUN make

FROM debian:11

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /

RUN apt-get update \
    && apt-get install -y \
            iptables \
            iproute2

COPY --from=builder /mmproxy/mmproxy /mmproxy

COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
