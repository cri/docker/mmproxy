#!/usr/bin/env bash

set -xeuo pipefail

MMPROXY_MARK="${MMPROXY_MARK:-123}"
MMPROXY_TABLE="${MMPROXY_MARK:-100}"

init() {
  set +e
  echo 1 > /proc/sys/net/ipv4/conf/eth0/route_localnet
  iptables -t mangle -I PREROUTING -m mark --mark "${MMPROXY_MARK}" -m comment --comment mmproxy -j CONNMARK --save-mark
  ip6tables -t mangle -I PREROUTING -m mark --mark "${MMPROXY_MARK}" -m comment --comment mmproxy -j CONNMARK --save-mark
  iptables -t mangle -I OUTPUT -m connmark --mark "${MMPROXY_MARK}" -m comment --comment mmproxy -j CONNMARK --restore-mark
  ip6tables -t mangle -I OUTPUT -m connmark --mark "${MMPROXY_MARK}" -m comment --comment mmproxy -j CONNMARK --restore-mark
  ip rule add fwmark "${MMPROXY_MARK}" lookup "${MMPROXY_TABLE}"
  ip -6 rule add fwmark "${MMPROXY_MARK}" lookup "${MMPROXY_TABLE}"
  ip route add local 0.0.0.0/0 dev lo table "${MMPROXY_TABLE}"
  ip -6 route add local ::/0 dev lo table "${MMPROXY_TABLE}"
  exit 0 # this is a hack so the init doesn't fail
}

run() {
  MMPROXY_ALLOWED_NETWORKS="${MMPROXY_ALLOWED_NETWORKS:-"0.0.0.0/0 ::/0"}"

  MMPROXY_DEST_PORT="${MMPROXY_DEST_PORT}"
  MMPROXY_DEST_PORT_4="${MMPROXY_DEST_PORT_4:-"${MMPROXY_DEST_PORT}"}"
  MMPROXY_DEST_PORT_6="${MMPROXY_DEST_PORT_6:-"${MMPROXY_DEST_PORT}"}"

  MMPROXY_DEST_IP_4="${MMPROXY_DEST_IP_4:-"127.0.0.1"}"
  MMPROXY_DEST_IP_6="${MMPROXY_DEST_IP_6:-"[::1]"}"

  for net in "${MMPROXY_ALLOWED_NETWORKS}"; do
    echo "${net}" >> /networks.txt
  done

  exec /mmproxy \
    --mark "${MMPROXY_MARK}" --table "${MMPROXY_TABLE}" \
    --allowed-networks /networks.txt \
    -l "0.0.0.0:${MMPROXY_LISTEN_PORT}" \
    -4 "${MMPROXY_DEST_IP_4}:${MMPROXY_DEST_PORT_4}" \
    -6 "${MMPROXY_DEST_IP_6}:${MMPROXY_DEST_PORT_6}" \
    "$@"
}

CMD="${1}"
shift

if [ "$CMD" = "mmproxy" ]; then
  run "$@"
elif [ "$CMD" = "init" ]; then
  init
else
  exec /bin/sh -c "$CMD $*"
fi
